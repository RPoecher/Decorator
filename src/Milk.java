
public class Milk extends CondimentDecorator{
	
	Beverage b;

	public Milk(Beverage s){
		this.b=s;
	}
	
	@Override
	public String getDescription() {
	
		return b.getdescription()+"Mocha";
	}

	@Override
	public double cost() {
		
		return b.cost()+1.20;
	}

}
