import java.io.*;

public class UperCaseInputStream extends BufferedInputStream{

	protected UperCaseInputStream(InputStream in) {
		super(in);
		// TODO Auto-generated constructor stub
	}

	public int read() throws IOException {
		int c = super.read();
		return (c == -1 ? c : Character.toUpperCase((char)c));
		
	}
}
