
public class Whip extends CondimentDecorator{

	
	Beverage b;

	public Whip(Beverage s){
		this.b=s;
	}
	
	@Override
	public String getDescription() {

		return b.getdescription()+"Whip";
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return b.cost()+2.30;
	}

}
